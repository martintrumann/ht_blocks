use ht_blocks::*;

#[test]
fn simple_tree() {
    let tree = vec![
        h1("Header"),
        h2("Info"),
        p("Here is some info"),
        div(vec![h3("here is a div"), p("and some info")]),
    ];

    assert_eq!(
        tree.to_html(),
        r#"<h1 id="header">Header</h1>
<h2 id="info">Info</h2>
<p>Here is some info</p>
<div>
<h3 id="here-is-a-div">here is a div</h3>
<p>and some info</p>
</div>"#
    )
}
