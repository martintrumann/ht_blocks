use std::fs::File;

use ht_blocks::*;

#[test]
fn parse_ron<'de>() {
    let blocks: Vec<Block> =
        ron::de::from_reader(File::open("tests/ron/simple.ron").unwrap()).unwrap();

    assert_eq!(
        blocks.to_html(),
        r#"<h1 id="hello">Hello</h1>
<p>This is a file I use for testing.</p>
<h2 id="lists">Lists</h2>
<ul>
<li>Item 1</li>
<li>Item 2</li>
<li>Item 3</li>
</ul>
<h2 id="images">Images</h2>
<img src="/path/to/image.png" alt="Here is an image">
<h2 id="divs">Divs</h2>
<div id="imgs">
<img src="/path/to/another-image.jpg" alt="Here is an image in a div">
<img src="/path/to/more-image.jpg" alt="Here is another image in a div">
</div>"#
    )
}

#[test]
fn full_tree() {
    let blocks: Vec<Block> =
        ron::de::from_reader(File::open("tests/ron/full.ron").unwrap()).unwrap();

    assert_eq!(
        blocks.to_html(),
        r#"<h1 id="full-test-page">Full test page.</h1>
<h2 id="divs">Divs</h2>
<div id="imgs">
<img src="/path/to/another-image.jpg" alt="Here is an image in a div">
<img src="/path/to/more-image.jpg" alt="Here is another image in a div">
</div>
<h2 id="paragraphs">Paragraphs</h2>
<p>This is a file I use for testing.</p>
<p>This is some text and <a href="/some/page">this is a link </a><code>and code</code>.</p>
<h2 id="lists">Lists</h2>
<ul>
<li>Some item</li>
<li>Some other item</li>
<li>Even more items</li>
</ul>
<ol>
<li>first item.</li>
<li>second item.</li>
<li>third item.</li>
</ol>
<h2 id="images">Images</h2>
<img src="/path/to/image.png" alt="Here is an image">
<img src="https://unsplash.com/photos/caH-ZLrisZA" alt="picture with external url">
<h2 id="table">Table</h2>
<table>
<tbody>
<tr>
<td>just</td>
<td>some</td>
<td>random</td>
<td>table</td>
</tr>
<tr>
<td>with</td>
<td>some</td>
<td>random</td>
<td>rows</td>
</tr>
<tr>
<td>and</td>
<td>stuff</td>
</tr>
</tbody>
</table>
<table>
<thead>
<tr>
<th>header</th>
<th>of this</th>
<th>random</th>
<th>table</th>
</tr>
</thead>
<tbody>
<tr>
<td>with</td>
<td>some</td>
<td>random</td>
<td>rows</td>
</tr>
<tr>
<td>and</td>
<td>stuff</td>
</tr>
</tbody>
</table>
<table>
<tbody>
<tr>
<td>just</td>
<td>some</td>
<td>random</td>
<td>table</td>
</tr>
<tr>
<td>with</td>
<td>some</td>
<td>random</td>
<td>rows</td>
</tr>
<tr>
<td>and</td>
<td>stuff</td>
</tr>
</tbody>
<tfoot>
<tr>
<td>and</td>
<td>a</td>
<td>footer</td>
</tr>
</tfoot>
</table>
<table>
<thead>
<tr>
<th>header</th>
<th>of this</th>
<th>random</th>
<th>table</th>
</tr>
</thead>
<tbody>
<tr>
<td>with</td>
<td>some</td>
<td>random</td>
<td>rows</td>
</tr>
<tr>
<td>and</td>
<td>stuff</td>
</tr>
</tbody>
<tfoot>
<tr>
<td>and</td>
<td>a</td>
<td>footer</td>
</tr>
</tfoot>
</table>
<h2 id="code">Code</h2>
<h3 id="hello-world-in-diffrent-languages">Hello world in diffrent languages.</h3>
<pre><code class="rust">fn main() {println!("hello world!")}</code></pre>
<pre><code class="js">console.log("hello world!");</code></pre>
<pre><code class="html">&lt;body&gt;
&lt;h1&gt;Hello world&lt;/h1&gt;
&lt;/body&gt;</code></pre>"#
    )
}
