use crate::{slug, Attributes, ToHtml};
use std::path::PathBuf;

mod level;
pub use level::Level;

mod table;
pub use table::{to_html, TableType};

/// Items that are used inside paragraphs.
#[derive(Debug, Clone, Serialize, Deserialize, PartialEq)]
pub enum InlineBlock {
    Text(String),
    Code(String),
    Link(PathBuf, String),
}

impl ToHtml for InlineBlock {
    fn to_html(&self) -> String {
        match self {
            InlineBlock::Text(t) => t.clone(),
            InlineBlock::Code(t) => ["<code>", t, "</code>"].concat(),
            InlineBlock::Link(url, t) => {
                ["<a href=\"", url.to_str().unwrap(), "\">", t, "</a>"].concat()
            }
        }
    }
}

impl From<Vec<InlineBlock>> for Block {
    fn from(v: Vec<InlineBlock>) -> Self {
        Self::Paragraph(v)
    }
}

/// Item that represents some element in html.
///
/// Easier to create using the helper functions.
#[derive(Debug, Clone, Serialize, Deserialize, PartialEq)]
pub enum Block {
    /// Div with attributes
    Div(Attributes, Vec<Block>),
    Header(Level, String),
    Paragraph(Vec<InlineBlock>),
    Image(PathBuf, String),
    UnorderedList(Vec<String>),
    OrderedList(Vec<String>),
    Table(TableType, Vec<Vec<String>>),
    Code(String, String),
}

impl Block {
    /// Adds an id to the block by wrapping it in a div.
    pub fn id(self, id: String) -> Self {
        Block::Div(Attributes::id(id), vec![self])
    }
}

impl From<String> for Block {
    fn from(s: String) -> Self {
        Self::Paragraph(vec![InlineBlock::Text(s)])
    }
}

impl From<Vec<Block>> for Block {
    fn from(v: Vec<Block>) -> Self {
        Self::Div(Default::default(), v)
    }
}

fn list(items: &[String]) -> String {
    if items.is_empty() {
        String::new()
    } else {
        items
            .iter()
            .map(|s| format!("\n<li>{}</li>", s))
            .collect::<Vec<_>>()
            .concat() + "\n"
    }
}

impl ToHtml for Block {
    fn to_html(&self) -> String {
        match self {
            Self::Div(a, c) => format!(
                "<div{}>\n{}\n</div>",
                a.get()
                    .map(|a| " ".to_owned() + &a.to_html())
                    .unwrap_or_default(),
                c.to_html()
            ),
            Self::Header(level, c) => {
                format!("<{level} id=\"{}\">{}</{level}>", slug(c), c)
            }
            Self::Paragraph(c) => {
                let items = c.iter().map(ToHtml::to_html).collect::<String>();
                ["<p>", &items, "</p>"].concat()
            }
            Self::Image(path, alt) => {
                format!("<img src=\"{}\" alt=\"{}\">", path.display(), alt)
            }
            Self::UnorderedList(items) => ["<ul>", &list(items), "</ul>"].concat(),
            Self::OrderedList(items) => ["<ol>", &list(items), "</ol>"].concat(),
            Self::Table(t, i) => table::to_html(t, i),
            Self::Code(lang, c) => [
                "<pre><code class=\"",
                lang,
                "\">",
                &c.replace(">", "&gt;").replace("<", "&lt;"),
                "</code></pre>",
            ]
            .concat(),
        }
    }
}

impl ToHtml for Vec<Block> {
    fn to_html(&self) -> String {
        self.iter()
            .map(ToHtml::to_html)
            .collect::<Vec<_>>()
            .join("\n")
    }
}
