/// A type for the table.
#[derive(Debug, Clone, Copy, PartialEq, Eq, Serialize, Deserialize)]
pub enum TableType {
    /// Both the header and the footer
    Both,
    /// Just the header
    Head,
    /// Just the footer
    Foot,
    /// A table with just the body.
    Neither,
}

impl TableType {
    /// Check if the table has a header
    pub fn head(&self) -> bool {
        matches!(self, Self::Both | Self::Head)
    }

    /// Set the table head
    pub fn set_head(&mut self, v: bool) {
        *self = match (v, &self) {
            (true, Self::Foot) => Self::Both,
            (true, Self::Neither) => Self::Head,
            (false, Self::Both) => Self::Foot,
            (false, Self::Head) => Self::Neither,
            _ => return,
        }
    }

    /// Check if the table has a footer
    pub fn foot(&self) -> bool {
        matches!(self, Self::Both | Self::Foot)
    }

    /// Set the table head
    pub fn set_foot(&mut self, v: bool) {
        *self = match (v, &self) {
            (true, Self::Head) => Self::Both,
            (true, Self::Neither) => Self::Foot,
            (false, Self::Both) => Self::Head,
            (false, Self::Foot) => Self::Neither,
            _ => return,
        }
    }
}

pub fn to_html(typ: &TableType, items: &[Vec<String>]) -> String {
    let mut out = String::from("<table>");
    if !items.is_empty() {
        out += "\n";

        let mut rows = items.iter().peekable();

        if typ.head() {
            out += "<thead>\n<tr>\n";
            for field in rows.next().unwrap() {
                out += &format!("<th>{}</th>\n", field)
            }
            out += "</tr>\n</thead>\n";
        }

        if rows.peek().is_some() {
            out += "<tbody>\n";

            let mut first = true;
            while let Some(row) = rows.next() {
                let row = [
                    "<tr>\n",
                    &row.iter()
                        .map(|field| format!("<td>{}</td>\n", field))
                        .collect::<Vec<String>>()
                        .concat(),
                    "</tr>\n",
                ]
                .concat();

                if rows.peek().is_none() {
                    if typ.foot() && !first {
                        out += &["</tbody>\n<tfoot>\n", &row, "</tfoot>\n"].concat();
                    } else {
                        out += &(row + "</tbody>\n");
                    }
                } else {
                    out += &row
                }

                first = false;
            }
        }
    }
    out + "</table>"
}

#[cfg(test)]
mod tests {
    use super::{to_html, TableType::*};

    #[test]
    fn empty() {
        let html = "<table></table>";
        assert_eq!(to_html(&Both, &Vec::new()), html);
        assert_eq!(to_html(&Head, &Vec::new()), html);
        assert_eq!(to_html(&Foot, &Vec::new()), html);
        assert_eq!(to_html(&Neither, &Vec::new()), html);
    }

    #[test]
    fn single_row() {
        let table = to_html(
            &Neither,
            &vec![vec!["this".into(), "is".into(), "here".into()]],
        );

        assert_eq!(
            table,
            r#"<table>
<tbody>
<tr>
<td>this</td>
<td>is</td>
<td>here</td>
</tr>
</tbody>
</table>"#
        )
    }

    #[test]
    fn single_row_header() {
        let html = r#"<table>
<thead>
<tr>
<th>this</th>
<th>is</th>
<th>here</th>
</tr>
</thead>
</table>"#;

        let table = to_html(
            &Both,
            &vec![vec!["this".into(), "is".into(), "here".into()]],
        );
        assert_eq!(table, html);
        let table = to_html(
            &Head,
            &vec![vec!["this".into(), "is".into(), "here".into()]],
        );
        assert_eq!(table, html);
    }

    #[test]
    fn single_row_footer() {
        let table = to_html(
            &Foot,
            &vec![vec!["this".into(), "is".into(), "here".into()]],
        );

        assert_eq!(
            table,
            r#"<table>
<tbody>
<tr>
<td>this</td>
<td>is</td>
<td>here</td>
</tr>
</tbody>
</table>"#
        )
    }

    #[test]
    fn with_head() {
        let table = to_html(
            &Head,
            &vec![
                vec!["This".into(), "is".into(), "head".into()],
                vec!["body".into(), "is".into(), "this".into()],
                vec!["and".into(), "also".into(), "this".into()],
            ],
        );

        assert_eq!(
            table,
            r#"<table>
<thead>
<tr>
<th>This</th>
<th>is</th>
<th>head</th>
</tr>
</thead>
<tbody>
<tr>
<td>body</td>
<td>is</td>
<td>this</td>
</tr>
<tr>
<td>and</td>
<td>also</td>
<td>this</td>
</tr>
</tbody>
</table>"#
        )
    }

    #[test]
    fn with_footer() {
        let table = to_html(
            &Foot,
            &vec![
                vec!["body".into(), "is".into(), "this".into()],
                vec!["and".into(), "also".into(), "this".into()],
                vec!["This".into(), "is".into(), "Foot".into()],
            ],
        );

        assert_eq!(
            table,
            r#"<table>
<tbody>
<tr>
<td>body</td>
<td>is</td>
<td>this</td>
</tr>
<tr>
<td>and</td>
<td>also</td>
<td>this</td>
</tr>
</tbody>
<tfoot>
<tr>
<td>This</td>
<td>is</td>
<td>Foot</td>
</tr>
</tfoot>
</table>"#
        )
    }

    #[test]
    fn with_both() {
        let table = to_html(
            &Both,
            &vec![
                vec!["Table".into(), "Head".into()],
                vec!["Table".into(), "Body".into()],
                vec!["More".into(), "Body".into()],
                vec!["Table".into(), "Foot".into()],
            ],
        );

        let html = r#"<table>
<thead>
<tr>
<th>Table</th>
<th>Head</th>
</tr>
</thead>
<tbody>
<tr>
<td>Table</td>
<td>Body</td>
</tr>
<tr>
<td>More</td>
<td>Body</td>
</tr>
</tbody>
<tfoot>
<tr>
<td>Table</td>
<td>Foot</td>
</tr>
</tfoot>
</table>"#;

        assert_eq!(table, html);
    }
}
