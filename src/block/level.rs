use std::str::FromStr;

/// All the levels of the header.
#[derive(Debug, PartialEq, Eq, PartialOrd, Ord, Clone, Copy, Serialize, Deserialize)]
pub enum Level {
    H1,
    H2,
    H3,
    H4,
    H5,
    H6,
}

impl FromStr for Level {
    type Err = ();
    fn from_str(s: &str) -> Result<Self, Self::Err> {
        let out = match s {
            "h1" | "H1" => Self::H1,
            "h2" | "H2" => Self::H2,
            "h3" | "H3" => Self::H3,
            "h4" | "H4" => Self::H4,
            "h5" | "H5" => Self::H5,
            "h6" | "H6" => Self::H6,
            _ => return Err(()),
        };

        Ok(out)
    }
}

impl std::fmt::Display for Level {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        f.write_str(match self {
            Self::H1 => "h1",
            Self::H2 => "h2",
            Self::H3 => "h3",
            Self::H4 => "h4",
            Self::H5 => "h5",
            Self::H6 => "h6",
        })
    }
}
