//! Simple functions for crating elements quickly.

use crate::{block::InlineBlock, Attributes, Block, Level, TableType};
use std::path::PathBuf;

/// Shorthand for a paragraph block.
pub fn p(s: impl Into<String>) -> Block {
    Block::Paragraph(vec![InlineBlock::Text(s.into())])
}

/// Shorthand for a div.
pub fn div(blocks: Vec<Block>) -> Block {
    Block::Div(Attributes::default(), blocks)
}
/// Shorthand for a div with attributes.
pub fn div_with_attributes(attributes: Attributes, blocks: Vec<Block>) -> Block {
    Block::Div(attributes, blocks)
}
/// Shorthand for a div with an id.
pub fn div_with_id(id: impl Into<String>, blocks: Vec<Block>) -> Block {
    Block::Div(Attributes::id(id.into()), blocks)
}
/// Shorthand for a div with a class.
pub fn div_with_class(class: impl Into<String>, blocks: Vec<Block>) -> Block {
    Block::Div(Attributes::class(class.into()), blocks)
}
/// Shorthand for a div with a vector of classes.
pub fn div_with_classes(classes: Vec<String>, blocks: Vec<Block>) -> Block {
    Block::Div(Attributes::classes(classes), blocks)
}

/// Shorthand for an image.
pub fn img(path: PathBuf, alt: impl Into<String>) -> Block {
    Block::Image(path, alt.into())
}

pub fn h1(s: impl Into<String>) -> Block {
    Block::Header(Level::H1, s.into())
}
pub fn h2(s: impl Into<String>) -> Block {
    Block::Header(Level::H2, s.into())
}
pub fn h3(s: impl Into<String>) -> Block {
    Block::Header(Level::H3, s.into())
}
pub fn h4(s: impl Into<String>) -> Block {
    Block::Header(Level::H4, s.into())
}
pub fn h5(s: impl Into<String>) -> Block {
    Block::Header(Level::H6, s.into())
}
pub fn h6(s: impl Into<String>) -> Block {
    Block::Header(Level::H6, s.into())
}

/// Table with only a body.
pub fn table(items: Vec<Vec<String>>) -> Block {
    Block::Table(TableType::Neither, items)
}
/// Table with a head
pub fn head_table(items: Vec<Vec<String>>) -> Block {
    Block::Table(TableType::Head, items)
}
/// Table with a footer
pub fn foot_table(items: Vec<Vec<String>>) -> Block {
    Block::Table(TableType::Foot, items)
}
/// Table with both head and footer
pub fn full_table(items: Vec<Vec<String>>) -> Block {
    Block::Table(TableType::Both, items)
}
