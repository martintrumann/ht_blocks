use crate::{slug, ToHtml};

/// Attributes of the element
#[derive(Debug, Default, Clone, Serialize, Deserialize, PartialEq)]
pub struct Attributes {
    id: Option<String>,
    classes: Vec<String>,
}

impl Attributes {
    /// Construct the attributes with this id.
    pub fn id(id: String) -> Self {
        Self {
            id: Some(id),
            ..Default::default()
        }
    }

    /// Construct the attributes with this class.
    pub fn class(class: String) -> Self {
        Self {
            classes: vec![class],
            ..Default::default()
        }
    }

    /// Construct the attributes with these class.
    pub fn classes(classes: Vec<String>) -> Self {
        Self {
            classes,
            ..Default::default()
        }
    }
}

impl Attributes {
    /// Get a refence to the id.
    pub fn get_id(&self) -> Option<&String> {
        self.id.as_ref()
    }

    /// Get a mutable refrence to the id.
    pub fn get_id_mut(&mut self) -> Option<&mut String> {
        self.id.as_mut()
    }

    /// Get a refence to classes.
    pub fn get_classes(&self) -> &Vec<String> {
        &self.classes
    }

    /// Get a mutable refence to classes.
    pub fn get_classes_mut(&mut self) -> &mut Vec<String> {
        &mut self.classes
    }

    /// Get the attributes if the value is non_zero
    pub fn get(&self) -> Option<&Self> {
        (self.id.is_some() || !self.classes.is_empty()).then(|| self)
    }

    /// Add a class to the list.
    pub fn add_class(&mut self, class: String) {
        self.classes.push(class);
    }

    pub fn remove_class(&mut self, i: usize) {
        self.classes.remove(i);
    }

    /// Set the attributes's id.
    pub fn romove_id(&mut self) {
        self.id = None;
    }

    /// Set the attributes's id.
    pub fn set_id(&mut self, id: String) {
        self.id = Some(id);
    }
}

impl ToHtml for Attributes {
    fn to_html(&self) -> String {
        let mut out = Vec::<String>::new();

        if let Some(id) = &self.id {
            out.push(format!("id=\"{}\"", slug(id)));
        }

        if !self.classes.is_empty() {
            out.push(format!(
                "class=\"{}\"",
                self.classes
                    .iter()
                    .map(slug)
                    .collect::<Vec<String>>()
                    .join(" ")
            ));
        }

        out.join(" ")
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn plain_id() {
        let attribute = Attributes::id("Header".into());
        assert_eq!(attribute.to_html(), r#"id="header""#);
    }

    #[test]
    fn plain_class() {
        let attribute = Attributes::class("Something".into());
        assert_eq!(attribute.to_html(), r#"class="something""#);
    }

    mod vec {
        use super::*;

        #[test]
        fn empty() {
            let attribute = Attributes::default();
            assert_eq!(attribute.to_html(), "");
        }

        #[test]
        fn one_id() {
            let attribute = Attributes::id("Id".into());
            assert_eq!(attribute.to_html(), "id=\"id\"");
        }

        #[test]
        fn one_class() {
            let attribute = Attributes::class("Only Class".into());
            assert_eq!(attribute.to_html(), "class=\"only-class\"");
        }

        #[test]
        fn multiple_classes() {
            let mut attribute = Attributes::class("Something".into());
            attribute.add_class("Something Else".into());
            assert_eq!(attribute.to_html(), r#"class="something something-else""#);
        }

        #[test]
        fn id_and_class() {
            let mut attribute = Attributes::id("Header".into());
            attribute.add_class("Something".into());
            assert_eq!(attribute.to_html(), r#"id="header" class="something""#);
        }

        #[test]
        fn multiple_classes_and_id() {
            let mut attribute = Attributes::id("Header".into());
            attribute.add_class("Something".into());
            attribute.add_class("Something Else".into());

            assert_eq!(
                attribute.to_html(),
                r#"id="header" class="something something-else""#
            );
        }
    }
}
