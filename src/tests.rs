mod code {
    use crate::{Block::Code, ToHtml};

    #[test]
    fn simple_code() {
        let code = Code("js".into(), "console.log(\"hi\");".into());
        assert_eq!(
            code.to_html(),
            r#"<pre><code class="js">console.log("hi");</code></pre>"#
        );
    }

    #[test]
    fn multiline_code() {
        let code = Code(
            "js".into(),
            r#"
    for (let i = 0; i < 100; i++) {
        console.log("test hi", i)
    }
"#
            .into(),
        );
        assert_eq!(
            code.to_html(),
            r#"<pre><code class="js">
    for (let i = 0; i &lt; 100; i++) {
        console.log("test hi", i)
    }
</code></pre>"#
        );
    }
}

mod header {
    use crate::{Block::Header, Level::*, ToHtml};

    #[test]
    fn simple_header() {
        assert!(H1 < H2);
        let header = Header(H1, "Header".into());
        assert_eq!(header.to_html(), r#"<h1 id="header">Header</h1>"#);
    }
}

mod image {
    use std::path::PathBuf;

    use crate::{Block::Image, ToHtml};

    #[test]
    fn simple() {
        let header = Image(PathBuf::from("/picture/here.jpg"), "Just a picture".into());
        assert_eq!(
            header.to_html(),
            r#"<img src="/picture/here.jpg" alt="Just a picture">"#
        );
    }

    #[test]
    fn empty_path() {
        let image = Image(PathBuf::from(""), "empty image".into());
        assert_eq!(image.to_html(), r#"<img src="" alt="empty image">"#);
    }

    #[test]
    fn empty_alt() {
        let image = Image(PathBuf::from("/some/image.png"), "".into());
        assert_eq!(image.to_html(), r#"<img src="/some/image.png" alt="">"#);
    }
}

mod list {
    use crate::{
        Block::{OrderedList, UnorderedList},
        ToHtml,
    };

    #[test]
    fn empty() {
        let list = UnorderedList(Vec::new());
        assert_eq!(list.to_html(), "<ul></ul>")
    }

    #[test]
    fn unordered() {
        let list = UnorderedList(vec!["here".into(), "is".into(), "a".into(), "list".into()]);
        assert_eq!(
            list.to_html(),
            r#"<ul>
<li>here</li>
<li>is</li>
<li>a</li>
<li>list</li>
</ul>"#
        )
    }

    #[test]
    fn ordered() {
        let list = OrderedList(vec!["here".into(), "is".into(), "a".into(), "list".into()]);
        assert_eq!(
            list.to_html(),
            r#"<ol>
<li>here</li>
<li>is</li>
<li>a</li>
<li>list</li>
</ol>"#
        )
    }
}

mod div {
    use crate::{
        Attributes,
        Block::{self, Div, Header, Paragraph},
        InlineBlock::{self, Text},
        Level::H1,
        ToHtml,
    };

    #[test]
    fn normal() {
        let div = Div(Default::default(), vec![Header(H1, "Header".into())]);

        let html = r#"<div>
<h1 id="header">Header</h1>
</div>"#;

        assert_eq!(div.to_html(), html);
    }

    #[test]
    fn multiple() {
        let div = Block::from(vec![
            Header(H1, "Header".into()),
            Paragraph(vec![Text("And some text as well.".into())]),
        ]);

        let html = r#"<div>
<h1 id="header">Header</h1>
<p>And some text as well.</p>
</div>"#;

        assert_eq!(div.to_html(), html);
    }

    #[test]
    fn multiple_with_id() {
        let div = Div(
            Attributes::id("Something".into()),
            vec![
                Header(H1, "Header".into()),
                Paragraph(vec![
                    Text("And some text as well.".into()),
                    InlineBlock::Code("with code".into()),
                ]),
            ],
        );

        let html = r#"<div id="something">
<h1 id="header">Header</h1>
<p>And some text as well.<code>with code</code></p>
</div>"#;

        assert_eq!(div.to_html(), html);
    }

    #[test]
    fn with_class() {
        let div = Div(
            Attributes::class("div".into()),
            vec![Header(H1, "Header".into())],
        );

        let html = r#"<div class="div">
<h1 id="header">Header</h1>
</div>"#;

        assert_eq!(div.to_html(), html);
    }
}
