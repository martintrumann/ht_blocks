//! Create html from Blocks
#[macro_use]
extern crate serde;

mod attribute;

pub use attribute::Attributes;

mod block;
pub use block::{Block, InlineBlock, Level, TableType};

mod helpers;
pub use helpers::*;

/// A trait allowing to turn structures to html.
pub trait ToHtml {
    fn to_html(&self) -> String;
}

use slug::slugify as slug;

#[cfg(test)]
mod tests;
